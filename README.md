**About the Project**

The project comprises of a combination of multi-modal and multi label image classifications and caption generation with the help of image descriptions.
The dataset used in the project for training the deep learning models is the PASCAL50S dataset.
The PASCAL50S dataset is generated using the 1000 images taken from the PASCAL VOC 2008 dataset.The base dataset contains 6000 images belonging to 20 image classes. 
50 images from each class are randomly sampled to form the 1000 images in PASCAL50S dataset and each image is annotated using 50 sentences.

The tasks accomplished in the project are:

* Multilabel classification of the images 
* Multi-modal image classification,where a combined representation of text and images is used for training, which is further used for image classification
* Image caption generation,by using the image and associated sentences for training the model
